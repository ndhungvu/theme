/*

[Main Script]

Project: KetNoiSo
Version: 1.0
Author : themelooks.com

*/

(function(b) {
    var p = b(window),
        h = b("body"),
        f = function(b, e) {
            return "undefined" === typeof b ? e : b
        };
    b(function() {
        b("[data-bg-img]").each(function() {
            var c = b(this);
            c.css("background-image", "url(" + c.data("bg-img") + ")").addClass("bg--img bg--overlay").attr("data-rjs", 2).removeAttr("data-bg-img")
        });
        retinajs();
        b('[data-toggle="tooltip"]').tooltip();
        b(".NavHoverIntent").hoverIntent({
            selector: "li.dropdown",
            over: function() {
                b(this).addClass("open")
            },
            out: function() {
                b(this).removeClass("open")
            },
            timeout: 0,
            interval: 0
        });
        var a = b(".AnimateScrollLink"),
            e = b(".AnimateScrollList"),
            r = function(c) {
                var a = b(this).attr("href");
                b(a).animatescroll({
                    padding: 0,
                    easing: "easeInOutExpo",
                    scrollSpeed: 1200
                });
                c.preventDefault()
            };
        a.on("click", r);
        e.on("click", "a", r);
        a = b('[data-popup="img"]');
        a.length && a.magnificPopup({
            type: "image",
            zoom: {
                enabled: !0
            }
        });
        b("[data-sticky]").each(function() {
            b(this).sticky({
                zIndex: 999
            })
        });
        a = b('[data-counter-up="numbers"]');
        a.length && a.counterUp({
            delay: 10,
            time: 1E3
        });
        b("[data-counter-down]").each(function() {
            var c = b(this);
            c.countdown(c.data("counter-down"), function(c) {
                b(this).html("<ul>" + c.strftime("<li><strong>%D</strong><span>Days</span></li><li><strong>%H</strong><span>Hours</span></li><li><strong>%M</strong><span>Minutes</span></li><li><strong>%S</strong><span>Seconds</span></li>") + "</ul>")
            })
        });
        b("[data-form-validation] form").each(function() {
            b(this).validate({
                errorPlacement: function() {
                    return !0
                }
            })
        });
        b(".owl-carousel").each(function() {
            var c =
                b(this);
            c.owlCarousel({
                items: f(c.data("carousel-items"), 1),
                margin: f(c.data("carousel-margin"), 0),
                loop: f(c.data("carousel-loop"), !0),
                smartSpeed: 1200,
                autoplay: f(c.data("carousel-autoplay"), !1),
                autoplayHoverPause: f(c.data("carousel-hover-pause"), !1),
                nav: f(c.data("carousel-nav"), !1),
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                dots: f(c.data("carousel-dots"), !1),
                dotsContainer: f(c.data("carousel-dots-container"), !1),
                responsive: f(c.data("carousel-responsive"), {})
            })
        });
        b(".banner--slider");
        a = b(".banner--slider-nav");
        e = a.find("li");
        a.length && e.css("width", 100 / e.length + "%");
        b(".services-tab--nav").find("li [title]").tooltip({
            trigger: "manual"
        }).end().find("li.active [title]").tooltip("show").end().on("shown.bs.tab", function(c) {
            b(c.target).tooltip("show");
            b(c.relatedTarget).tooltip("hide")
        });
        a = b("#pricingIncluded");
        if (a.length) {
            var k = a.find(".pricing--item:first-child .pricing--features li:not(:first-child)");
            a.find(".pricing--item:not(:first-child) .pricing--features li").each(function(c) {
                c =
                    b(this);
                var a = k.eq(c.index()).text();
                c.prepend('<strong class="hidden-md hidden-lg">' + a + "</strong>")
            })
        }
        var a = b(".PricingTable"),
            q = a.find("thead th");
        a.length && a.find("tbody td").each(function() {
            var c = b(this),
                a = q.eq(c.index()).text();
            c.append('<strong class="hidden-md hidden-lg">' + a + "</strong>")
        });
        var l = b("#vpsPricing"),
            m, h, v, d, w, x;
        m = l.find("#vpsPricingSlider");
        h = l.find("[data-put-value]");
        v = l.find("[data-put-href]");
        d = function(c) {
            d.value = 1;
            d.max = c.length - 1;
            d.changeValue = function(a, e) {
                d.value = b.isEmptyObject(e) ?
                    d.value : e.value;
                m.find(".ui-slider-handle").html('<div class="pricing--content"><div class="pricing--header bg-color--black"><h3 class="h4">' + c[d.value].title + '</h3><h4 class="h5">Starting At<strong>' + c[d.value].price + '</strong></h4></div><div class="pricing--icon"><i class="fa ' + c[d.value].icon + '"></i></div></div>');
                h.each(function() {
                    var a = b(this);
                    a.text(c[d.value][a.data("put-value")])
                });
                v.attr("href", c[d.value][v.data("put-href")])
            };
            m.slider({
                animate: "fast",
                range: "min",
                min: 0,
                max: d.max,
                value: d.value,
                step: 1,
                create: d.changeValue,
                slide: d.changeValue
            })
        };
        l.length && b.getJSON("json/vps-plans.json", d).done(function() {
            w = l.find(".vps-pricing--items");
            x = l.find(".vps-pricing--tag");
            x.css("height", w.height());
            p.on("resize", function() {
                x.css("height", w.height())
            })
        });
        var y = b(".datacenter--slider");
        y.on("changed.owl.carousel", function(b) {
            y.find(".owl-item").eq(b.item.index).children("a").tab("show")
        });
        var g = b("#map"),
            a = 400 < b("#contactInfo").outerHeight() ? b("#contactInfo").outerHeight() + 60 : 400,
            e = function() {
                var b =
                    new google.maps.Map(g[0], {
                        center: {
                            lat: g.data("map-latitude"),
                            lng: g.data("map-longitude")
                        },
                        zoom: g.data("map-zoom"),
                        scrollwheel: !1,
                        disableDefaultUI: !0,
                        zoomControl: !0,
                        styles: [{
                            featureType: "all",
                            elementType: "geometry.fill",
                            stylers: [{
                                weight: "2.00"
                            }]
                        }, {
                            featureType: "all",
                            elementType: "geometry.stroke",
                            stylers: [{
                                color: "#9c9c9c"
                            }]
                        }, {
                            featureType: "all",
                            elementType: "labels.text",
                            stylers: [{
                                visibility: "on"
                            }]
                        }, {
                            featureType: "landscape",
                            elementType: "all",
                            stylers: [{
                                color: "#f2f2f2"
                            }]
                        }, {
                            featureType: "landscape",
                            elementType: "geometry.fill",
                            stylers: [{
                                color: "#ffffff"
                            }]
                        }, {
                            featureType: "landscape.man_made",
                            elementType: "geometry.fill",
                            stylers: [{
                                color: "#ffffff"
                            }]
                        }, {
                            featureType: "poi",
                            elementType: "all",
                            stylers: [{
                                visibility: "off"
                            }]
                        }, {
                            featureType: "road",
                            elementType: "all",
                            stylers: [{
                                saturation: -100
                            }, {
                                lightness: 45
                            }]
                        }, {
                            featureType: "road",
                            elementType: "geometry.fill",
                            stylers: [{
                                color: "#eeeeee"
                            }]
                        }, {
                            featureType: "road",
                            elementType: "labels.text.fill",
                            stylers: [{
                                color: "#7b7b7b"
                            }]
                        }, {
                            featureType: "road",
                            elementType: "labels.text.stroke",
                            stylers: [{
                                color: "#ffffff"
                            }]
                        }, {
                            featureType: "road.highway",
                            elementType: "all",
                            stylers: [{
                                visibility: "simplified"
                            }]
                        }, {
                            featureType: "road.arterial",
                            elementType: "labels.icon",
                            stylers: [{
                                visibility: "off"
                            }]
                        }, {
                            featureType: "transit",
                            elementType: "all",
                            stylers: [{
                                visibility: "off"
                            }]
                        }, {
                            featureType: "water",
                            elementType: "all",
                            stylers: [{
                                color: "#46bcec"
                            }, {
                                visibility: "on"
                            }]
                        }, {
                            featureType: "water",
                            elementType: "geometry.fill",
                            stylers: [{
                                color: "#c8d7d4"
                            }]
                        }, {
                            featureType: "water",
                            elementType: "labels.text.fill",
                            stylers: [{
                                color: "#070707"
                            }]
                        }, {
                            featureType: "water",
                            elementType: "labels.text.stroke",
                            stylers: [{
                                color: "#ffffff"
                            }]
                        }]
                    });
                if ("undefined" !== typeof g.data("map-marker")) {
                    var a = g.data("map-marker"),
                        d = 0;
                    for (d; d < a.length; d++) new google.maps.Marker({
                        position: {
                            lat: a[d][0],
                            lng: a[d][1]
                        },
                        map: b,
                        animation: google.maps.Animation.DROP,
                        draggable: !0
                    })
                }
            };
        g.length && (g.css("height", a).next("#contactInfo").css("margin-top", -b("#contactInfo").outerHeight()).prev("#map").css("z-index", -1), e());
        var a = b(".contact--form"),
            e = b(".contact--map"),
            z = b(".contact--status"),
            r = function(a) {
                var c =
                    b(a);
                a = c.attr("action");
                c = c.serialize();
                b.post(a, c, function(b) {
                    z.show().html(b).delay(3E3).fadeOut("show")
                })
            };
        a.length && (e.children("#map").css("height", a.outerHeight()), a.find("form").validate({
            errorPlacement: function() {
                return !0
            },
            submitHandler: r
        }));
        var t = b("#contactInfo").find(".contact-info--item"),
            u = 0;
        t.length && t.each(function(a) {
            var c = b(this);
            u = c.outerHeight() > u ? c.outerHeight() : u;
            t.length === a + 1 && t.css("height", u)
        });
        b(".product--single-img").on("click", ".owl-item a", function() {
            b(this).addClass("active").parent(".owl-item").siblings().children("a").removeClass("active")
        });
        var a = b(".product--quantity"),
            n = a.children("input"),
            A = parseInt(n.data("min"), 10),
            B = parseInt(n.data("max"), 10);
        a.on("click", "[data-up]", function() {
            var a = b(this),
                a = parseInt(n.val(), 10) + a.data("up");
            a <= B && n.val(a)
        }).on("click", "[data-down]", function() {
            var a = b(this),
                a = parseInt(n.val(), 10) - a.data("down");
            a >= A && n.val(a)
        });
        a = b("#productRatingSelect");
        a.length && a.barrating({
            theme: "bootstrap-stars",
            hoverState: !1
        });
        a = b(".coming-soon--slider");
        a.length && a.css("height", b(".coming-soon--content").outerHeight()).trigger("refresh.owl.carousel");
        "undefined" !== typeof b.cColorSwitcher && b.cColorSwitcher({
            switcherTitle: "Main Colors:",
            switcherColors: [{
                bgColor: "#119ee6",
                filepath: "css/colors/theme-color-1.css"
            }, {
                bgColor: "#8bc34a",
                filepath: "css/colors/theme-color-2.css"
            }, {
                bgColor: "#ff5252",
                filepath: "css/colors/theme-color-3.css"
            }, {
                bgColor: "#ff9600",
                filepath: "css/colors/theme-color-4.css"
            }, {
                bgColor: "#e91e63",
                filepath: "css/colors/theme-color-5.css"
            }, {
                bgColor: "#00BCD4",
                filepath: "css/colors/theme-color-6.css"
            }, {
                bgColor: "#FC5143",
                filepath: "css/colors/theme-color-7.css"
            }, {
                bgColor: "#00B249",
                filepath: "css/colors/theme-color-8.css"
            }, {
                bgColor: "#D48B91",
                filepath: "css/colors/theme-color-9.css"
            }, {
                bgColor: "#8CBEB2",
                filepath: "css/colors/theme-color-10.css"
            }],
            switcherTarget: b("#changeColorScheme")
        })
    });
    p.on("load", function() {
        var a = function() {
            1 < p.scrollTop() ? h.addClass("isScrolling") : h.removeClass("isScrolling")
        };
        a();
        p.on("scroll", a);
        a = b(".AdjustRow");
        a.length && a.isotope({
            layoutMode: "fitRows"
        });
        a = b(".MasonryRow");
        a.length && a.isotope();
        var e = b(".features-grid--left"),
            f = b(".features-grid--right"),
            k = 0,
            a = function() {
                f.find(".features-grid--item").each(function(a) {
                    var m = b(this);
                    k = m.outerHeight() > k ? m.outerHeight() : k;
                    f.children(".row").children("div").css("height", k);
                    a === f.length - 1 && e.css("height", f.outerHeight())
                })
            };
        a();
        p.on("resize", a);
        var q = b(".gallery--items"),
            a = b(".gallery--filter");
        q.length && (q.isotope({
            animationEngine: "best-available",
            itemSelector: ".gallery--item"
        }), a.on("click", "li", function() {
            var a = b(this),
                e = a.data("target");
            q.isotope({
                filter: "*" !== e ? '[data-cat~="' + e + '"]' : e
            });
            a.addClass("active").siblings().removeClass("active")
        }));
        a = b("#preloader");
        a.length && a.fadeOut()
    })
})(jQuery);